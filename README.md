# PowerDNS Docker images for arm64 (aarch64)

Since it's apparently difficult to get newer versions (that include Prometheus metrics) of PowerDNS, I wanted to take advantage
of newer Ubuntu versions' inclusions. That's what this is.

Images live in this repo's registry and should be freely downloadable. Much of the Docker image's contents are yoinked straight
from the [upstream pdns repo](https://github.com/powerdns/pdns).
